## Présentation

Exercices pour travailler les commandes basiques du terminal (création/suppression de dossiers/fichiers, permissions).

L'interaction se fait avec `curl` en ligne de commande, les solutions sont envoyées sous forme de fichier zip et vérifiées par le serveur.



## Usage

Lancer le serveur avec `python serveur.py`

Accéder au premier exercice depuis le terminal avec `curl localhost:8080` (ou l'IP du serveur depuis une autre machine).

Si on accède au serveur depuis un navigateur, on obtient un tableau des exercices faits et du nombre d'essais pour chaque IP qui s'est connectée (informations stockées dans `userdata.pickle` entre les exécutions, qu'on supprime quand on veut).

Usage local et temporaire seulement, `app.run` est un serveur de test.

## Ajouter des exercices

Il suffit d'ajouter la consigne `consignes/exo{i}.txt`, la solution `solutions/exo{i}.zip` et de modifier `NB_EXOS` dans `serveur.py`. On peut ajouter `i` à `EXOS_CHECK_PERM` si on veut que les permissions des fichiers/dossiers soient vérifiées.