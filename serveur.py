from flask import Flask, render_template, request, send_from_directory
from collections import defaultdict
import zipfile
import pickle

app = Flask(__name__)

NB_EXOS = 7
EXOS_CHECK_PERM = {4, 5, 7}
noms_exos = [f"exo{i}.zip" for i in range(1, NB_EXOS + 1)]


def check(solution_name, submit_stream, checkperm):
    message = ""

    def fail(erreur):
        red_error = f"\033[1;91m{erreur}\033[0m"
        return False, message + red_error

    submit_zip = zipfile.ZipFile(submit_stream)
    solution_zip = zipfile.ZipFile("solutions/" + solution_name)
    submit_infos = {f.filename: f for f in submit_zip.infolist()}
    solution_infos = {f.filename: f for f in solution_zip.infolist()}
    for filename in submit_infos:
        if filename not in solution_infos:
            return fail(f"{filename} est en trop dans votre archive.\n")
        submit_perm = oct(submit_infos[filename].external_attr >> 16)[-3:]
        solution_perm = oct(solution_infos[filename].external_attr >> 16)[-3:]
        if checkperm and submit_perm != solution_perm:
            return fail(f"Les permissions sur {filename} sont incorrectes.\n")
        else:
            green_OK = "\033[1;92mOK\033[0m"
            message += f"{filename} {green_OK}\n"
    for filename in solution_infos:
        if filename not in submit_infos:
            return fail(f"Il manque {filename} dans votre archive.\n")
    return True, message


@app.route("/", methods=["GET"])
def acces_get():
    ip = request.remote_addr
    useragent = request.headers.get("User-Agent", "")
    if "curl" not in useragent:
        return render_template("scoreboard.html", userdata=userdata, nb_exos=NB_EXOS)
    else:
        exo_suivant = min(
            i for i in range(NB_EXOS + 2) if not userdata[ip][i]["success"]
        )
        if exo_suivant < NB_EXOS:
            fichier_consigne = f"consignes/exo{exo_suivant+1}.txt"
        else:
            fichier_consigne = f"consignes/fin.txt"
        return open(fichier_consigne).read()


@app.route("/", methods=["POST"])
def acces_post():
    ip = request.remote_addr
    rendu = request.files["file"]
    if rendu.filename not in noms_exos:
        return f"\033[1;91mPas d'exercice disponible avec pour nom de fichier {rendu.filename}\n\033[0m"
    else:
        exo_id = noms_exos.index(rendu.filename)
        is_success, message = check(
            rendu.filename, rendu.stream, exo_id in EXOS_CHECK_PERM
        )
        if not userdata[ip][exo_id]["success"]:
            userdata[ip][exo_id]["tries"] += 1
        if is_success and not (userdata[ip][exo_id]["success"]):
            userdata[ip][exo_id]["success"] = True
            message += f"Bravo vous avez réussi l'exercice {exo_id+1} en {userdata[ip][exo_id]['tries']} essais.\n"
            with open("userdata.pickle", "wb") as f:
                pickle.dump(userdata, f)
        return message


@app.route('/downloads/<path:filename>', methods=['GET'])
def download(filename):
    return send_from_directory("downloads", filename)

def default_exo_info():
    return {"success": False, "tries": 0}

def default_ip_dict():
    return defaultdict(default_exo_info)

if __name__ == "__main__":
    try:
        userdata = pickle.load(open("userdata.pickle", "rb"))
    except FileNotFoundError:
        # ip -> (exo_id -> {"success": ?, "tries": ?}})
        userdata = defaultdict(default_ip_dict)
    app.run("0.0.0.0", 8080)
